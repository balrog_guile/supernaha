<?php
/* =============================================================================
 * データベース用のjsonをつくる
 * php dbmigration.phar import mysql://root:root@127.0.0.1:8889/supernaha_devel /Volumes/Untitled/site/super-naha/src/databaseSchema/table_name.json
 * php dbmigration.phar migrate mysql://root:root@127.0.0.1:8889/supernaha_devel mysql://root:root@127.0.0.1:8889/supernaha
 * @Author
 * ========================================================================== */
$tableName = 'table_name';
$json =  [
    //platform - 固定で触らない
    'platform' => 'mysql',
    
    //テーブルリスト
    'table' =>[
        
        /////テーブル
        $tableName => [
            
            //カラムリスト
            'column' => [
                
                //ID
                'id' => [
                    "type" =>  "integer",
                    "default"=>  null,
                    "notnull"=>  true,
                    "unsigned"=>  true,
                    "autoincrement"=>  true,
                    "comment" => 'ID',
                    "platformOptions"=>  [],
                    "customSchemaOptions" => []
                ],
                
                //name
                'name' => [
                    "type" =>"string",
                    "default" => null,
                    "notnull" => false,
                    "length" =>255,
                    "comment" => null,
                    "comment" => 'お名前',
                    "platformOptions" => [
                        "collation" => "utf8mb4_unicode_ci"
                    ],
                    "customSchemaOptions" => []
                ],
                
                //kana
                'kana' => [
                    "type" =>"string",
                    "default" => null,
                    "notnull" => false,
                    //"length" =>255,
                    "comment" => null,
                    "comment" => 'かな',
                    "platformOptions" => [
                        "collation" => "utf8mb4_unicode_ci"
                    ],
                    "customSchemaOptions" => []
                ],
                
            ],
            
            
            
            //インデックス
            "index" =>  [
                "PRIMARY" => [
                    "column"=> [
                        "id"
                    ],
                    "primary" => true,
                    "unique" => true,
                    "option"=> [
                        "lengths"=> [
                            null
                        ]
                    ]
                ]
            ],
            
            ////オプション項目
            "foreign" => [],
            
            "trigger"=> [],
            
            "option"=>  [
                    "engine"=> "InnoDB",
                    "collation" =>"utf8mb4_unicode_ci",
                    "autoincrement" =>"1",
                    "comment"=> "",
                    "create_options"=> [],
                    "charset"=> "utf8mb4"
            ],
            
            
        ],
    ],
    
    "view" => []
];

$path = __DIR__.'/../ryunosukemigrate/test-supernaha.json';
$jsonSrc = json_decode( file_get_contents( $path ), true );

foreach( $json['table'] as $tableName => $src ){
    $jsonSrc['table'][$tableName] = $src;
}
foreach( $json['view'] as $tableName => $src ){
    $jsonSrc['view'][$tableName] = $src;
}

$path = __DIR__.'/../databaseSchema/'.$tableName.'.json';
file_put_contents( $path, json_encode( $jsonSrc, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT ) );