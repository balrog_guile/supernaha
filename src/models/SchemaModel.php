<?php
/* =============================================================================
 * schema定義モデル
 * @Author
 * ========================================================================== */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace app\models;

use Yii;
use yii\base\Model;
use app\superNaha\Behavior\SchemaBehavior;


/**
 * Description of SchemaModel
 *
 * @author itoukiyonori
 * @property string $new_table_name 新規追加テーブル名
 */
class SchemaModel extends Model
{
    
    // ----------------------------------------------------
    /**
     * フィールドを定義
     */
    
    // テーブル名
    public $new_table_name;
    
    /////フィールド用
    //フィールド名
    public $field_name;
    public $comment;
    public $default;
    public $nullable;
    public $type;
    public $null;
    public $unsigned;
    public $charset;
    public $collation;
    public $auto_increment;//autoincrement
    public $basic_data_type;//基本データ型
    public $data_length;//データ長
    public $insert_datetime;//インサート時に日付時刻自動入力
    public $updat_datetime;//アップデート時に日付時刻自動入力
    public $is_primary;//主キー
    public $is_index;//索引
    public $is_null_ok;//NULLOK
    public $under_point;//小数点以下の桁数




    // ----------------------------------------------------
    /**
     * プロパティ
     */
    /////システムデフォルトテーブルにて利用禁止名一覧
    public $ignoreTableNames = [
        'ImageManager',
        'migration',
        'profile',
        'social_account',
        'token',
        'user'
    ];
    
    // ----------------------------------------------------
    /**
     * behaviorをアタッチ
     */
    public function behaviors(): array {
        $return = parent::behaviors();
        $return['SchemaBehavior'] = SchemaBehavior::className();
        
        return $return;
    }
    
    // ----------------------------------------------------
    /**
     * ルール
     */
    public function rules(){

        $rules = [];

        //新規追加時
        if( $this->getScenario() == 'newTable' ){
            $rules[] = [ ['new_table_name'], 'required'];
            $rules[] = [ ['new_table_name'], 'trim'];
            $rules[] = [ ['new_table_name'], 'string', 'length' => [4, 24]];
            $rules[] = [ ['new_table_name'], 'validIsEnabeTableName' ];
        }

        return $rules;
    }

    // ----------------------------------------------------
    /**
     * ラベル
     */
    public function attributeLabels()
    {
        return [
            //新規用
            'new_table_name' => Yii::t('app', '追加テーブル名'),
            
            //フィールド定義用
            'auto_increment' => Yii::t('app', '自動採番'),
            'field_name' => Yii::t('app', 'フィールド名'),
            'comment' => Yii::t('app', 'コメント'),
            'default' => Yii::t('app', 'デフォルト値'),
            'nullable' => Yii::t('app', 'NULL(空の値を許可する)'),
            'is_null_ok' => Yii::t('app', 'NULL(空の値を許可する)'),
            'type' => Yii::t('app', 'データ型'),
            'unsigned' => Yii::t('app', '符号なし(負の値なし)'),
            'charset' => Yii::t('app', '文字コード'),
            'collation' => Yii::t('app', '文字コード照合順'),
            'data_length' =>  Yii::t('app', 'データ長'),
            'insert_datetime' =>  Yii::t('app', 'インサート時に日付時刻自動入力'),
            'updat_datetime' =>  Yii::t('app', 'アップデート時に日付時刻自動入力'),
            'basic_data_type' =>  Yii::t('app', 'データ型'),
            'is_primary' =>  Yii::t('app', '主キー'),
            'is_index'=>  Yii::t('app', 'インデックス'),
            'under_point'=>  Yii::t('app', '小数点以下の桁数'),
        ];
    }
    // ----------------------------------------------------
    /**
     * シナリオ追加
     */
     public function scenarios()
     {
         return [
             
             //新規テーブル作成時
             'newTable' => ['new_table_name'],
             
             //フィールド設定
             'fields' => [
                 'field_name', 'comment', 'default', 'nullable', 'type',
                'unsigned', 'charset', 'collation', 'auto_increment',
                 'data_length', 'insert_datetime',  'updat_datetime',
                 'basic_data_type', 'is_primary', 'is_index', 'is_null_ok',
                 'under_point',
             ],
             
         ];
     }
     
      // ----------------------------------------------------
    /**
     * 使えるテーブル銘菓チェック
     * @param string $tableName 
     */
    public function isEnabeTableName( $tableName ){
        return !in_array( $tableName, $this->ignoreTableNames );
    }
    
    // ----------------------------------------------------
    /**
     * スキーマ情報をjsonから取得
     * @return null|false|object nullの場合ファイルなし false json_decode失敗 obnject成功
     */
    public function getSchemaFromJson(){
        $path = Yii::getAlias( '@app/databaseSchema/schema.json' );
        if( !file_exists($path) ){
            return null;
        }
        return json_decode( file_get_contents( $path ) );
    }
    
    
    // ----------------------------------------------------
    /**
     * 管理画面用のJSON調整
     * @return null|false|object nullの場合ファイルなし false json_decode失敗 obnject成功
     */
    public function getAdminSchemaParse(){
        
        $json = $this->getSchemaFromJson();
        if( $json === null || $json === false ){
            return $json;
        }
        
        /////インデックス分析
        $indexList = [];
        foreach( $json->tables as $tableName => $table ){
            $indexList[ $tableName ] = [
                'PRIMARY' => null,
                'normal' => [],
            ];
            foreach( $table->indexes as $indexName => $values ){
                if( $indexName == 'PRIMARY' ){
                    $indexList[ $tableName ]['PRIMARY'] = $values->columns[0];
                }
                else {
                    $indexList[ $tableName ]['normal'][] = $values->columns[0];
                }
            }
        }
        
        
        ////フィールド分析
        foreach( $json->tables as $tableName => $table ){
            
            foreach( $table->columns as $columnName => $values ){
                
                //NULL許可
                if( isset($values->nullable) &&$values->nullable === true ){
                    $json->tables->{$tableName}->columns->{$columnName}->{'is_null_ok'} = 1;
                }
                else {
                    $json->tables->{$tableName}->columns->{$columnName}->{'is_null_ok'} = 0;
                }
                
                
                //主キー
                if( $indexList[ $tableName ]['PRIMARY'] == $columnName ){
                    $json->tables->{$tableName}->columns->{$columnName}->{'is_primary'} = 1;
                }
                else {
                    $json->tables->{$tableName}->columns->{$columnName}->{'is_primary'} = 0;
                }
                
                //インデックス
                if(in_array($columnName, $indexList[ $tableName ]['normal']) ){
                    $json->tables->{$tableName}->columns->{$columnName}->{'is_index'} = 1;
                }
                else {
                    $json->tables->{$tableName}->columns->{$columnName}->{'is_index'} = 0;
                }
                
                //オートインクリメント
                if( isset($values->autoIncrement) &&$values->autoIncrement === true ){
                    $json->tables->{$tableName}->columns->{$columnName}->{'auto_increment'} = 1;
                }
                else {
                    $json->tables->{$tableName}->columns->{$columnName}->{'auto_increment'} = 0;
                }
                
                
                //データ型を分解
                if( $values->type == 'text' ){
                    $values->basic_data_type = '文字列';
                    $values->data_length = '';
                    
                }
                if( $values->type == 'longtext' ){
                    $values->basic_data_type = '文字列大きなデータ';
                    $values->data_length = '';
                }
                if( $values->type == 'date' ){
                    $values->basic_data_type = '日付';
                    $values->data_length = '';
                }
                if( $values->type == 'time' ){
                    $values->basic_data_type = '時刻';
                    $values->data_length = '';
                }
                if( $values->type == 'datetime' ){
                    $values->basic_data_type = '日付+時刻';
                    $values->data_length = '';
                }
                if( preg_match( '/varchar/i', $values->type ) ){
                    $values->basic_data_type = '文字列固定長型';
                    $match = [];
                    preg_match( '/\(([0-9]{1,})\)/', $values->type, $match );
                    if( isset($match[1]) ){
                        $values->data_length = $match[1];
                    }
                    else {
                        $values->data_length = '';
                    }
                }
                if( preg_match( '/decimal/i', $values->type ) ){
                    if(!preg_match( '/unsigned/', $values->type ) ){
                        $values->basic_data_type = '小数';
                    }
                    else {
                        $values->basic_data_type = '小数(マイナスなし)';
                    }
                    $match = [];
                    preg_match( '/\(([0-9]{1,})\,([0-9]{1,})\)/', $values->type, $match );
                    if( isset($match[1]) ){
                        $values->data_length = $match[1];
                    }
                    else {
                        $values->data_length = '';
                    }
                    if( isset($match[2]) ){
                        $values->under_point = $match[2];
                    }
                    else {
                        $values->under_point = '';
                    }
                    
                }
                
                if( preg_match( '/biignt/i', $values->type ) ){
                    if(!preg_match( '/unsigned/', $values->type ) ){
                        $values->basic_data_type = '整数(大きな値)';
                    }
                    else {
                        $values->basic_data_type = '整数(大きな値,マイナスなし)';
                    }
                    
                    $match = [];
                    preg_match( '/\(([0-9]{1,})\)/', $values->type, $match );
                    if( isset($match[1]) ){
                        $values->data_length = $match[1];
                    }
                    else {
                        $values->data_length = '';
                    }
                }
                else if( preg_match( '/int/i', $values->type ) ){
                    if(!preg_match( '/unsigned/', $values->type ) ){
                        $values->basic_data_type = '整数';
                    }
                    else {
                        $values->basic_data_type = '整数(マイナスなし)';
                    }
                    
                    $match = [];
                    preg_match( '/\(([0-9]{1,})\)/', $values->type, $match );
                    if( isset($match[1]) ){
                        $values->data_length = $match[1];
                    }
                    else {
                        $values->data_length = '';
                    }
                }
                
                
                
                
            }
            
        }
        
        return $json;
    }
    
    
   // ----------------------------------------------------
    /**
     *  バリデーション: 使えるテーブル名か？
     */
    public function validIsEnabeTableName($attribute, $params, $validator){
        
        //システム利用テーブル
        if( ! $this->isEnabeTableName( $this->$attribute ) ){
            $this->addError($attribute, '利用できないテーブル名です');
        }
        
         //定義済みを新規追加しようとする
        if( $this->getScenario() == 'newTable' ){
            $keys = array_keys( (array)$this->getSchemaFromJson()->tables );
            if( in_array( $this->$attribute , $keys ) ){
                $this->addError($attribute, '定義済みテーブル名です');
            }
        }
        
        //テーブル名として問題ないか
        if( preg_match( '/^[0-9]/', $this->$attribute ) ){
            $this->addError($attribute, '頭文字に数字を使うことはできません');
        }
        
        
    }
    
    // ----------------------------------------------------
}
