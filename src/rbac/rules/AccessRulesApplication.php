<?php
/* =============================================================================
 * アクセスルール
 * ========================================================================== */
namespace app\rbac\rules;

use Yii;
use yii\filters\AccessRule;

class AccessRulesApplication extends AccessRule
{
    static public $target = [];
    /**
     * @inheritdoc
     * */
    protected function matchRole($user)
    {
        $userId = Yii::$app->user->getId();

        //ロール指定がなければOK
        if (empty($this->roles)) {
            return true;
        }
        
        //ログインしてないのは問答無用
        if(is_null($userId)){
            return false;
        }


        //とりあえず管理者だけ
        return true;

        
        //許可されているアイテム
        // 以下ユーザー権限階級が必要な倍
        $authItems = [];
        foreach( Yii::$app->authManager->getItemsByUser($userId) as $row ) {
            $authItems[] = $row->name;
        }

        foreach ($this->roles as $role) {
            if ($role === 'investor') {
                if( in_array( 'investor', $authItems  ) ){
                    return true;
                }
            }
            elseif ($role === 'asset') {
                if( in_array( 'asset', $authItems  ) ){
                    return true;
                }
            }
            elseif ($role === 'administrator') {
                if( in_array( 'administrator', $authItems  ) ){
                    return true;
                }
            }
        }
        
        
        return false;
    }

}
