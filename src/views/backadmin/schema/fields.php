<?php
/* =============================================================================
 * フィールド定義
 * @Author
 * ========================================================================== */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\SchemaAssets;
use app\superNaha\helper\VariablesHelper;
SchemaAssets::register($this);

/* @var $this yii\web\View */
/* @var $newTableModel app\models\SchemaModel */
/* @var $form yii\widgets\ActiveForm */
?>

<h1>フィールド定義:<?= $tableName ?></h1>

<?php $form = ActiveForm::begin(); ?>
<div class="panel panel-default viewmodel">
    <div class="panel-heading">
        フィールド定義
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-12">
                
                
                <!--一覧表示 -->
                <table class="table table-striped fileds-table">
                    <thead>
                        <tr>
                            <th>主キー/自動採番</th>
                            <th>フィールド名</th>
                            <th>データ型/データ長:小数点以下のケタ</th>
                            <th>コメント</th>
                            <th>削除</th>
                        </tr>
                    </thead>
                    <tbody class="sortable-tr">
                        <tr>
                            <td>
                                <div class="detail-panel hide">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <?= $form->field($model, 'insert_datetime[]')->checkbox() ?>
                                            <hr>
                                            <?= $form->field($model, 'updat_datetime[]')->checkbox() ?>
                                            <hr>
                                            <?= $form->field($model, 'is_null_ok[]')->checkbox() ?>
                                            <hr>
                                            <?= $form->field($model, 'is_index[]')->checkbox() ?>
                                            <hr>
                                            <?= $form->field($model, 'collation[]')->textInput(['maxlength' => true]) ?>
                                            <hr>
                                            <?= $form->field($model, 'default[]')->textarea(['rows' => 4])?>
                                        </div>
                                    </div>
                                </div>
                                <?= $form->field($model, 'is_primary[]')->checkbox([])->label(false)  ?>
                                <?= $form->field($model, 'auto_increment[]')->checkbox([])->label(false)  ?>
                                <?= Html::a( '詳細設定', 'javascript:void(0);', ['class' => 'btn  btn-block  btn-yellow400_rsd btn-xs open-detail-panel'] ) ?>
                            </td>
                            <td>
                                <?= $form->field($model, 'field_name[]')->textInput(['maxlength' => true])->label(false) ?>
                            </td>
                            <td>
                                <?= $form->field($model, 'basic_data_type[]')->dropDownList(VariablesHelper::getBasicDataTypeList())->label(false) ?>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <?= $form->field($model, 'data_length[]')->textInput(['maxlength' => true])->label(false) ?>
                                    </div>
                                    <div class="col-xs-6">
                                        <?= $form->field($model, 'under_point[]')->textInput(['maxlength' => true])->label(false) ?>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <?= $form->field($model, 'comment[]')->textInput(['maxlength' => true])->label(false) ?>
                            </td>
                            <td>
                                <?= Html::a( '削除', 'javascript:void(0);', ['class' => 'btn btn-danger btn-block btn-xs delete-btn'] ) ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!--一/覧表示 -->
                
            </div>
        </div>
    </div>
    <div  class="panel-footer">
        <a href="javascript:void(0);" class="btn btn-lightblue700_rsd add-field ">フィールド追加</a>
    </div>
</div>



 <?= Html::submitInput( '登録', [ 'class' => 'btn btn-block btn-blue800_rsd'] ) ?>
<?php ActiveForm::end(); ?>

<script>
    var table = <?= json_encode($table)?>;
    var fields = <?=    json_encode( $fields ) ?>;
    var fieldNameList = <?= json_encode( $fieldNameList ) ?>;
</script>
