<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\superNaha\helper\Url;
/* @var $this yii\web\View */
/* @var $newTableModel app\models\SchemaModel */
/* @var $form yii\widgets\ActiveForm */
?>

<h1>テーブル定義</h1>

<!--
<div class="panel panel-default">
    <div class="panel-heading">
        操作
    </div>
    <div  class="panel-body">
        <div class="row">
            <div class="col-xs-12">
                <a href="<?= Url::to('/backadmin/schema/relation', true ) ?>" class="btn btn-purple600_rsd">リレーション定義</a>
            </div>
        </div>
    </div>
</div>
-->
<div class="panel panel-default">
    <div class="panel-heading">
        テーブル一覧
    </div>
    <div  class="panel-body">
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>テーブル名</th>
                            <th width="20%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach( $tableList as $tableName ): ?>
                            <tr>
                                <td><?= $tableName ?></td>
                                <td>
                                    <?= Html::a(
                                            'フィールド定義', 
                                           Url::to( ['/backadmin/schema/fields', 'table_name' => $tableName ], true ),
                                            [ 'class' => 'btn  btn-pink400_rsd btn-block' ]
                                        )
                                    ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <hr>
        <?php $form = ActiveForm::begin(); ?>
            <div class="row">
                <div class="col-xs-6">
                    <?= $form->field($newTableModel, 'new_table_name')->textInput(['placeholder' => '追加テーブル名'])->label(false) ?>
                </div>
                <div class="col-xs-2">
                    <?= Html::submitButton( '新規作成', [ 'class' => 'btn btn-block btn-md btn-pink600_rsd' ]  ) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="panel-footer">
    </div>
</div>
