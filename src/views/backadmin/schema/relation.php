<?php
/* =============================================================================
 * リレーション定義
 * @Author
 * ========================================================================== */
/*
 * @var $this yii\web\View 
 * @var $form yii\widgets\ActiveForm 
 */

use app\assets\RelationAsset;
RelationAsset::register($this);


?>
<h1>リレーション定義</h1>

<div class="panel panel-default">
    <div class="panel-heading">
        Tables・Views
    </div>
    <div class="panel-body">
        <div class="container">
            <div class="row">
                <div class="col-xs-4">
                    テーブル名1
                </div>
                <div class="col-xs-4">
                    テーブル名1
                </div>
            </div>
        </div>
    </div>
    <!--
    <div class="panel-footer">
        パネルのフッター
    </div>
    -->
</div>



<div class="relation-define-area">
    
    <div class="like-table">
        <h4>テーブル</h4>
        <div class="like-column">
            カラム1
        </div>
        <div class="like-column">
            カラム2
        </div>
        <div class="like-column">
            カラム3
        </div>
    </div>
    
    <div class="like-table">
        <h4>テーブル</h4>
        <div class="like-column">
            カラム1
        </div>
        <div class="like-column">
            カラム2
        </div>
        <div class="like-column">
            カラム3
        </div>
    </div>
    
    
</div>