<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\superNaha\helper\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    
    <!--
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'About', 'url' => ['/site/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>
    -->
    <!-- header -->
    <header>
        <div class="row">
            <div class="col-xs-2 logo-area">
                SuperNaha
            </div>
            <div class="col-xs-10">
            </div>
        </div>
    </header>
    
    
    
    <div class="container-fluid main-frame">
        <div class="row">
            <div class="col-xs-2 left-navi">
                <div class="row">
                    <div class="xol-xs-12">
                        <div class="list-group">
                            <a class="list-group-item" href="<?= Url::to('/backadmin/schema/', [] ) ?>">
                                <h4 class="list-group-item-heading"><i class="material-icons">build</i> スキーマ定義</h4>
                            </a>
                            <a class="list-group-item" href="<?= Url::to('/backadmin/schema/', [] ) ?>">
                                <p class="list-group-item-text">テーブル定義</p>
                            </a>
                            <a class="list-group-item active" href="<?= Url::to('/backadmin/schema/relation', [] ) ?>">
                                <p class="list-group-item-text">リレーション定義</p>
                            </a>
                            <a class="list-group-item" href="javascript:void(0);">
                                <h4 class="list-group-item-heading"><i class="material-icons">person</i> ユーザー</h4>
                            </a>
                            <a class="list-group-item" href="javascript:void(0);">
                                <p class="list-group-item-text">ユーザー定義</p>
                            </a>
                            <a class="list-group-item" href="javascript:void(0);">
                                <p class="list-group-item-text">ユーザー権限管理</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-10 main-contents-area">
                <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
