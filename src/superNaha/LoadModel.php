<?php
/* =============================================================================
 * モデルをロードする
 * @Author
 * ========================================================================== */
namespace app\superNaha;

use Yii;
use app\models\UserModel;
use app\models\UserSearchModel;

class LoadModel {
    
    //クラス定義を保存
    public static $classNameList = [];
    
    // ----------------------------------------------------
    /**
     * モデルクラスを定義
     * @return string 基本モデル名
     */
    public static function getMode( $table ){
        
        $controller = Yii::$app->controller;
        
         //すでにある
        if( isset(self::$classNameList[ $table ]) ){
            $className = self::$classNameList[ $table ]['className'];
            return $className;
        }
        
        ////新規作成
        $className = 'DYNAMICMODEL_' . $table . '_' . time().'_'.rand(1,2000).'_';
        self::$classNameList[ $table ] = [
            'className' => $className
        ];
        
        
        //基本モデルを作成
        $path = __DIR__.'/ModelsTemplate/Model.php';
        $content = file_get_contents( $path );
        $content = str_replace( 'TTTHHH__modelname__TTTHHH', $className, $content );
        $content = str_replace( 'TTTHHH__rules__TTTHHH', json_encode([]), $content );
        $content = str_replace( 'TTTHHH__labels__TTTHHH', json_encode([]), $content );
        $content = str_replace( 'TTTHHH__tablename__TTTHHH', $table, $content );
        file_put_contents( __DIR__.'/TempModels/'.$className.'Model.php', $content );
        
        
        
        //クエリモデルを作成
        $path = __DIR__.'/ModelsTemplate/Query.php';
        $content = file_get_contents( $path );
        $content = str_replace( 'TTTHHH__modelname__TTTHHH', $className, $content );
        $content = str_replace( 'TTTHHH__rules__TTTHHH', json_encode([]), $content );
        $content = str_replace( 'TTTHHH__labels__TTTHHH', json_encode([]), $content );
        $content = str_replace( 'TTTHHH__tablename__TTTHHH', $table, $content );
        file_put_contents( __DIR__.'/TempModels/'.$className.'Query.php', $content );
        
        //クエリモデルを作成
        $path = __DIR__.'/ModelsTemplate/SearchModel.php';
        $content = file_get_contents( $path );
        $content = str_replace( 'TTTHHH__modelname__TTTHHH', $className, $content );
        $content = str_replace( 'TTTHHH__rules__TTTHHH', json_encode([]), $content );
        $content = str_replace( 'TTTHHH__labels__TTTHHH', json_encode([]), $content );
        $content = str_replace( 'TTTHHH__tablename__TTTHHH', $table, $content );
        file_put_contents( __DIR__.'/TempModels/'.$className.'SearchModel.php', $content );
        
        return $className;
    }
    
    
    // ----------------------------------------------------
    /**
     * 一覧用
     */
    public static function LoadSearchModel( $table ){
        
        $controller = Yii::$app->controller;
        
        //$model = new ActiveSearchModel();
        //$dataProvider = $model->search(Yii::$app->request->queryParams);
        
        /////クラス定義
        $className = self::getMode($table, 'search');
        $classNameBasic= 'app\\superNaha\\TempModels\\' .$className.'Model';
        $classNameSearch= 'app\\superNaha\\TempModels\\' .$className.'SearchModel';
        
        $searchModel = new $classNameSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        
        return [
            'model' => new $classNameBasic(),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ];
    }
    // ----------------------------------------------------
    
}
