<?php
/* =============================================================================
 *  フロント用の拡張コントローラー
 * @Author
 * ========================================================================== */
namespace app\superNaha;
use Yii;
use yii\filters\AccessControl;
use app\superNaha\LoadModel;

class Controller extends \yii\web\Controller
{
    
    //テンプレートのパス
    public $templatePath = '';
    
    
    // ----------------------------------------------------
    /**
     * 初期化
     */
    public function init() {
        parent::init();
        global $kiyotchi_routing;
        $this->templatePath = $kiyotchi_routing->templatePath;
    }
    
    // ----------------------------------------------------
    /**
     * 実行後
     */
    public function afterAction($action, $result){
        $result = parent::afterAction($action, $result);
        
        //終わったモデル処理を削除
        if(is_array( LoadModel::$classNameList ) ){
            foreach(  LoadModel::$classNameList  as $table => $datas ){
                $path1 =  __DIR__.'/TempModels/'.$datas['className'].'Model.php';
                $path2 =  __DIR__.'/TempModels/'.$datas['className'].'Query.php';
                $path3 =  __DIR__.'/TempModels/'.$datas['className'].'SearchModel.php';
                unlink( $path1 );
                unlink( $path2 );
                unlink( $path3 );
            }
        }
        
        return $result;
    }
    
    // ----------------------------------------------------
}
