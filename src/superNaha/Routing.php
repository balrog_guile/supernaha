<?php
/* =============================================================================
 * ビューページからのルーティングを可能にする
 * ============================================================================ */
namespace superNaha;

class Routing {
    
    public $isHtmlCall = false;
    public $templatePath = '';
    
    // ----------------------------------------------------
    /**
     * コンストラクタ
     */
    public function __construct() {
        
        global $config;
        
        //URLパース
        $currentUrl = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
        $parse = parse_url($currentUrl);//path
        $list = explode( '/', $parse['path'] );
        array_shift( $list );
        $path = implode( '/', $list );
        if( $path == '' ){ $path = 'index'; }
        
        
        //末尾がdetailだったら詳細ページ
        
        //末尾がindexだったら一覧ページ
        
        
        
        //存在チェック(ディレクトリの場合）
        $checkPath = __DIR__.'/../web/'.$path.'/index.php';
        if(file_exists($checkPath)){
            $this->isHtmlCall = true;
            $this->templatePath = realpath($checkPath);
            $config['components']['urlManager']['rules'] = [
                $parse['path'] => 'site/index'
            ];
            return;
        }
        
        //存在チェック(ファイルの場合）
        $checkPath = __DIR__.'/../web/'.$path.'.php';
        if(file_exists($checkPath)){
            $this->isHtmlCall = true;
            $this->templatePath = realpath($checkPath);
            $config['components']['urlManager']['rules'] = [
                $parse['path'] => 'site/index'
            ];
            return;
        }
        
    }
    
    // ----------------------------------------------------
    
}
$kiyotchi_routing = new \superNaha\Routing;