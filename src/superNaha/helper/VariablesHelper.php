<?php
/* =============================================================================
 * スーパー那覇値
 * @Author
 * ========================================================================== */
namespace app\superNaha\helper;

/**
 * Description of ValueHelper
 *
 * @author itoukiyonori
 */
class VariablesHelper {
    
    // ----------------------------------------------------
    /**
     *  データ・タイプ基本型リスト
     * @param boolean  $undef 未選択を追加するか？
     * @return array
     */
    public static function getBasicDataTypeList( $undef = false ){
        
        $list = [
            '文字列', '文字列固定長型', '文字列大きなデータ',
            '整数', '整数(マイナスなし)', '小数', '小数(マイナスなし)',
            '整数(大きな値)', '整数(大きな値,マイナスなし)',
            '日付+時刻', '日付', '時刻', '画像', 'ファイル',
        ];
        $return = [];
        if( $undef ){ $return[''] = '未選択'; }
        foreach( $list as $one ){
            $return[ $one ] = $one;
        }
        
        return $return;
    }
    
    // ----------------------------------------------------
    
    
}
