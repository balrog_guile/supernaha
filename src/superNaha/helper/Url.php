<?php
/* =============================================================================
 * スーパー那覇用URLヘルパ
 * @Author
 * ========================================================================== */
namespace app\superNaha\helper;
class Url extends \yii\helpers\Url{
    
    // ----------------------------------------------------
    /**
     * 現在のURLを取得
     */
    public static function currentUrl(){
        return (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
        
    }
    
    // ----------------------------------------------------
    /**
     * テーマフォルダを取得
     */
    public static function getThemeDirectory(){
        return self::base( true ).'/web/';
    }
    
    // ----------------------------------------------------
    
}
