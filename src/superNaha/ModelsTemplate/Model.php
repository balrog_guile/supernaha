<?php
namespace app\superNaha\TempModels;
use Yii;
class TTTHHH__modelname__TTTHHHModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%TTTHHH__tablename__TTTHHH}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $json = 'TTTHHH__rules__TTTHHH';
        return json_decode($json);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        $json = 'TTTHHH__labels__TTTHHH';
        return json_decode($json);
    }

    /**
     * {@inheritdoc}
     * @return ConfigQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TTTHHH__modelname__TTTHHHQuery(get_called_class());
    }
}
