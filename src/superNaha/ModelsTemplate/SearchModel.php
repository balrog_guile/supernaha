<?php
namespace app\superNaha\TempModels;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\superNaha\TempModels\TTTHHH__modelname__TTTHHHModel;

class TTTHHH__modelname__TTTHHHSearchModel extends TTTHHH__modelname__TTTHHHModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $json = 'TTTHHH__rules__TTTHHH';
        return json_decode($json);
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TTTHHH__modelname__TTTHHHModel::find();
        // add conditions that should always apply here
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
             $query->where('0=1');
            return $dataProvider;
        }
        $query->where('0=1');
        
        return $dataProvider;
    }
}
