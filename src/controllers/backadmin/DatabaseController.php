<?php
/* =============================================================================
 * データベース操作
 * @Author
 * ========================================================================== */
namespace app\controllers\backadmin;

use Yii;
use yii\db\Schema;

class DatabaseController extends \yii\web\Controller
{
    // ----------------------------------------------------
    /**
     * 初期処理
     */
    public function init() {
        parent::init();
    }
    
    // ----------------------------------------------------
    /**
     * 最初のページ
     * @return type
     */
    public function actionIndex()
    {
        
        
        
        return $this->render('index');
    }
    
    
    // ----------------------------------------------------
}
