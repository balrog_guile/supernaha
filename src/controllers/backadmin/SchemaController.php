<?php
/* =============================================================================
 * DBスキーマを管理
 * @Author
 * ========================================================================== */
namespace app\controllers\backadmin;

use Yii;

use ngyuki\DbdaTool\Console\ConfigLoader;
use ngyuki\DbdaTool\DataSource\DataSourceFactory;
use ngyuki\DbdaTool\SchemaFilter;

use ngyuki\DbdaTool\Comparator;
use ngyuki\DbdaTool\Console\Application;
use ngyuki\DbdaTool\DataSource\ConnectionSourceInterface;
use ngyuki\DbdaTool\SqlGenerator\MySqlGenerator;
use ngyuki\DbdaTool\SqlGenerator\PseudoGenerator;

use app\superNaha\Behavior\SchemaBehavior;
use app\models\SchemaModel;

class SchemaController extends \yii\web\Controller
{
    
    // ----------------------------------------------------
    /**
     * ビヘイビア定義
     */
    public function behaviors()
    {
        return [
            
            //schema用
            'SchemaBehavior' => SchemaBehavior::className(),
            
        ];
    }
    
    
    
    // ----------------------------------------------------
    /**
     * テーブル管理
     */
    public function actionIndex()
    {
        //SchemaJSファイル
        $path = Yii::getAlias( '@app/databaseSchema/schema.json' );
        $newTableModel = new SchemaModel();
        $newTableModel->setScenario('newTable');
        
        
        /////ファイルの存在をチェック
        $schema = $newTableModel->getAdminSchemaParse();
        
        
        /////追加テーブルの実行
        if( $newTableModel->load(Yii::$app->request->post() ) && $newTableModel->validate() ){
        }
        
        
        /////jsonデータ分析
        $tableList = [];
        foreach( $schema-> tables as $table => $schema ){
            if( $newTableModel->isEnabeTableName( $table ) ){
                $tableList[ $table ] = $table;
            }
        }
        
        return $this->render(
            'index',
            [
                'schema' => $schema,
                'tableList' => $tableList,
                'newTableModel' => $newTableModel,
            ]
        );
    }
    
    
    // ----------------------------------------------------
    /**
     * フィールドスキーム
     */
    public function actionFields( $table_name = null ){
        
        if( is_null( $table_name ) ){
            return $this->redirect(['index']);
        }
        
        $model = new SchemaModel();
        $model->setScenario('fields');
        $schema = $model->getAdminSchemaParse();
        $fields = [];
        if( isset( $schema->tables->{$table_name} ) ){
            $table = $schema->tables->{$table_name};
            foreach( $schema->tables->{$table_name}->columns as $fieldName => $detail ){
                $set = (array)$detail;
                $set['field_name'] = $fieldName;
                $fields[] = $set;
            }
        }
        $fieldNameList = array_keys( $model->attributes );
        
        
        return $this->render(
            'fields',
            [
                'schema' => $schema,
                'model' => $model,
                'tableName' => $table_name,
                'table' => $table,
                'fields' => $fields,
                'fieldNameList' => $fieldNameList,
            ]
        );
    }
    
    
    // ----------------------------------------------------
    /**
     * diffをとる
     */
    public function actionDiff(){
        
        $path = Yii::getAlias('@app/config/db.php');
        $config = require $path;
        $dataSourceFactory = new DataSourceFactory($config);
        
        
        //メイン
         $target= $dataSourceFactory->create('@');
        
        
        //json
        $path = Yii::getAlias( '@app/databaseSchema/schema.json' );
        $source = $dataSourceFactory->create($path);
        
        //$dataSourceFactory2 = json_decode( file_get_contents($path) );
        //$target = $dataSourceFactory->create('@');
        
        
        $filter = (new SchemaFilter())->setIgnoreTablePattern([]);
        
        $diff = (new Comparator())->compare(
            $filter->filter($target->getSchema()),
            $filter->filter($source->getSchema())
        );
        
        $generator = null;
        
        if ($target instanceof ConnectionSourceInterface) {
            $generator = new MySqlGenerator($target->getConnection());
        } else
        if ($source instanceof ConnectionSourceInterface) {
            $generator = new MySqlGenerator($source->getConnection());
        } else {
            $generator = new PseudoGenerator();
        }
        $sqls = $generator->diff($diff);
        var_dump($sqls);
        
        return true;
    }
    
    // ----------------------------------------------------
    /**
     * リバースエンジニアリング
     */
    public function actionReverse(){
        
        $path = Yii::getAlias('@app/config/db.php');
        $config = require $path;
        
        $dataSourceFactory = new DataSourceFactory($config);
        $source = $dataSourceFactory->create('@');
        $filter = (new SchemaFilter())->setIgnoreTablePattern([]);
        $tables = $filter->filter($source->getSchema());
        $dump = json_encode($tables, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) . "\n";
        
        $path = Yii::getAlias( '@app/databaseSchema/schema.json' );
        file_put_contents( $path, $dump );
        
        return true;
    }
    
    // ----------------------------------------------------
    /**
     * リレーション定義
     */
    public function actionRelation()
    {
        
        
        
        return $this->render('relation');
    }
    // ----------------------------------------------------

}
