<?php
/* =============================================================================
 * webページコントローラー
 * @Author
 * ========================================================================== */
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\superNaha\Controller;
use app\superNaha\ClassBuilder;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;


class SiteController extends Controller
{
    
    // ----------------------------------------------------
    /**
     * 初期処理
     */
    public function init() {
        parent::init();
    }
    
    
    // ----------------------------------------------------
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    // ----------------------------------------------------
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    // ----------------------------------------------------
    /**
     * とにかくここにくる
     *
     * @return string
     */
    public function actionIndex(){
        
        return $this->renderFile( $this->templatePath );
    }
    
    // ----------------------------------------------------
}
