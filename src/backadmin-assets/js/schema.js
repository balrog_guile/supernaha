/* =============================================================================
 * schemaページ用
 * @Author
 * ========================================================================== */
(function($){$(document).ready(function(){
    
    
    // ----------------------------------------------------
    /**
     * ViewModel
     */
    //初期化データ
    var model = {
        'fields': fields,
    };
    var viewModelOuter;
    
    
    /////インデックス用リピート
    var indexFieldList = {};
    var manifestChildIndex = {
        ui: indexFieldList,
        init: function( $node, runtime ){
        }
    };
    
    
    ///////////// フィールドリピート用動作定義
    var isCheckbox = [ 'is_null_ok', 'is_primary', 'is_index', 'auto_increment' ];
    var uiFieldsList = {};
    
    
    
    //設定フィールド
    for( var x = 0; x < fieldNameList.length; x ++ ){
        if( $.inArray( fieldNameList[x], isCheckbox ) >= 0 ){ continue; }
        uiFieldsList[  '[name="SchemaModel[' + fieldNameList[x] + '][]"]' ] = {
            bind: fieldNameList[x]
        };
    }
    
    //チェックボックスのものを初期化
    for( var x =0 ;x < isCheckbox.length; x ++ ){
        (function( theOne ){
            uiFieldsList[  '[name="SchemaModel[' + theOne + '][]"][type=checkbox]' ]  = {
                init: function( $node, value ){
                    if( value['data'][theOne] == 1 ){
                        $node.attr( 'checked', 'checked' );
                    }
                }
            };
        })(isCheckbox[x]);
    }
    
    //データ長のreadonly
    uiFieldsList[  '[name="SchemaModel[data_length][]"]' ]['watch'] = '[name="SchemaModel[basic_data_type][]"]';
    uiFieldsList[  '[name="SchemaModel[data_length][]"]' ]['bind'] =  function( data, value ){
        var $control = this.my['find']('[name="SchemaModel[data_length][]"]');
        $control.val( data['data_length'] );
        var checkArray = [ '文字列', '文字列大きなデータ', '日付', '時刻', '日付+時刻', '画像', 'ファイル' ];
        if( $.inArray( this.my['find']('[name="SchemaModel[basic_data_type][]"]').val(), checkArray ) >= 0 ){
            $control.attr('readonly', 'readonly');
            data['data_length'] = '';
            $control.val('');
        }
        else{
            $control.removeAttr('readonly');
        }
    };
    
    //小数点以下のreadonly
    uiFieldsList[  '[name="SchemaModel[under_point][]"]' ]['watch'] = '[name="SchemaModel[basic_data_type][]"]';
    uiFieldsList[  '[name="SchemaModel[under_point][]"]' ]['bind'] =  function( data, value ){
        var $control = this.my['find']('[name="SchemaModel[under_point][]"]');
        $control.val( data['under_point'] );
        var checkArray = [ '小数', '小数(マイナスなし)' ];
        if( $.inArray( this.my['find']('[name="SchemaModel[basic_data_type][]"]').val(), checkArray ) < 0 ){
            $control.attr('readonly', 'readonly');
            data['under_point'] = '';
            $control.val('');
        }
        else{
            $control.removeAttr('readonly');
        }
    };
    
    
    //主キー
    
    
    
    //ダイアログ制御
    uiFieldsList['.open-detail-panel'] = {
        'bind': function( data, value ){
            var index  = this.my.index();
            var $control = this.my['find']('.open-detail-panel');//コントローラーの特定
            //初期化のときだけ
            if( value == null ){return;}
            //クリック時
            this.my['find']('.detail-panel').dialog('open');
        }
    };
    
    
    //削除
    uiFieldsList['.delete-btn'] = {
        'bind':  function( data, value ){
            var index  = this.my.index();
            var $control = this.my['find']('.open-detail-panel');//コントローラーの特定
            //初期化のときだけ
            if( value == null ){return;}
            this.my.remove();
        }
    };
    
    
    
    //アタッチ
    var manifestChild = {
        ui: uiFieldsList,
        init: function( $node, runtime ){
            this.my['find']('.detail-panel').dialog({
                        autoOpen: false,
                        modal: true,
                        width: $(document).width() * 0.8,
                        height: $(document).height() * 0.8,
                        appendTo: this.my['find']('td:eq(0)'),//←bodyに追加されるとうまく動かない
                        buttons:[
                            {
                                text: '　　OK　　',
                                'class':'btn btn-blue800_rsd',
                                click: function() {
                                    $(this).dialog('close');
                                }
                            },
                        ],
            });
            this.my['find']('.detail-panel').removeClass('hide');
        }
    };
    
    
    ///////////// メインViewMode;
    var manifest = {
            data: model,
            init: function ($form, form) {
                viewModelOuter = this;
            },
            ui: {
                /////リピート
                'table.fileds-table>tbody': {
                    'list': 'tr',
                    'bind': 'fields',
                    'manifest': manifestChild
                },
                /////追加
                '.add-field': function (data, val, elem){
                    if( val == null ){ return; }
                    this.my.insert( 'table>tbody', {} );
                }
            }
    };
    $('.viewmodel').my( manifest);
    
    // ----------------------------------------------------
    /**
     * そーたブル
     */
    $('.sortable-tr').sortable({
        'items': 'tr',
        'helper': 'clone',
    });
    
    // ----------------------------------------------------
    /** 
     *  viewModelテスト
     */
    /*
    var model= { 
        'test': 'テスト',
        'fields': [
            { 'name': '名前1', 'id': 1 },
            { 'name': '名前2', 'id': 2 },
        ]
    };
    
    var manifestChild = {
        ui: {
            '[data-bind="name"]': 'name',
            '[data-bind="id"]': 'id'
        },
        init: function( $node, runtime ){
        }
    };
    
    var viewModelOuter;
    var manifest = {
            data: model,
            init: function ($form, form) {
                viewModelOuter = this;
            },
            ui: {
                
                //単発
                '[name="test"]': 'test',
                
                //リピート
                'table>tbody': {
                    'list': 'tr',
                    'bind': 'fields',
                    'manifest': manifestChild
                },
                
                //追加
                '[data-bind-add]': function (data, val, elem){
                    if( val == null ){ return; }
                    var set = {
                            'name': $(elem).attr('data-bind-val-name'),
                            'id': $(elem).attr('data-bind-val-id'),
                    };
                    console.log(data);
                    this.my.insert( 'table>tbody', set );
                }
                
            }
    };
    
    //var test = $('.set-test').my( manifest, model );
    
    $('.test').on(
            'click',
            function(e){
                viewModelOuter.my.insert( 'table>tbody', {'name': '外部追加', 'id': 'test'}  );
                alert(1);
            }
    );
    */
    // ----------------------------------------------------
    
    
    
    
    
    
    
});})(jQuery);