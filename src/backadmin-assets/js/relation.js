/* =============================================================================
 * リレーション定義ページ用
 * @Author
 * ========================================================================== */
(function($){$(document).ready(function(){
    
    // ----------------------------------------------------
    /**
     * 共通利用
     * @type {type}
     */
    //リレーションのライン
    var lineList = {};
    
    // ----------------------------------------------------
    /**
     * ジョイントするやつ
     */
     var jointer = new $().jointer('black','red', true);
    
    // ----------------------------------------------------
    /**
     * ドラッがぶる
     */
    $('.relation-define-area .like-table').draggable({
        start: function( event, ui ) {
            if( typeof(lineList['one']) != 'undefined' ){
                lineList['one'].position();
            }
        },
        drag: function( event, ui ) {
            if( typeof(lineList['one']) != 'undefined' ){
                lineList['one'].position();
            }
        },
        stop: function( event, ui ) {
            if( typeof(lineList['one']) != 'undefined' ){
                lineList['one'].position();
            }
        },
    });
    $('.relation-define-area .like-table').resizable();
    $('.relation-define-area .like-table .like-column').draggable({
        revert: true,
        helper: function(){
            return $('<div class="drag-pointer"></div>');
        },
        
    });
    
    // ----------------------------------------------------
    /**
     * スクロールしたとき
     */
    $('.relation-define-area').on("scroll", function(evt){
            if( typeof(lineList['one']) != 'undefined' ){
                lineList['one'].position();
            }
        });
    
    // ----------------------------------------------------
    /**
     * ドロップしたとき
     */
    $('.relation-define-area .like-table .like-column').droppable({
        drop: function( event, ui ) {
            lineList['one'] =  new LeaderLine({start: $(ui.draggable).get(0), end: $(this).get(0) });
        }
    });
    
    // ----------------------------------------------------
    
    
    
    
});})(jQuery);